package com.oniyidewhite.android.pizzalocator.adapter;

/**
 * Created by android on 22/09/2017.
 */

import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.oniyidewhite.android.pizzalocator.fragment.ScreenSlidePageFragment;
import com.oniyidewhite.android.pizzalocator.model.PizzaPlace;

/**
 * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
 * sequence.
 */
public class ScreenSlidePageAdapter extends FragmentStatePagerAdapter {

    private PizzaPlace place;

    public ScreenSlidePageAdapter(FragmentManager fm, PizzaPlace place) {
        super(fm);
        this.place = place;
    }

    @Override
    public Fragment getItem(int position) {

        ScreenSlidePageFragment screenSlidePageFragment = new ScreenSlidePageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", place.getImages().get(position));
        screenSlidePageFragment.setArguments(bundle);
        return screenSlidePageFragment;
    }

    @Override
    public int getCount() {
        return place.getImages().size();
    }
}