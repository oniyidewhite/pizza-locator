package com.oniyidewhite.android.pizzalocator.helper;

import com.oniyidewhite.android.pizzalocator.model.PizzaPlace;

import java.util.ArrayList;

/**
 * Helper to retrive only the essential information from Place API
 * Created by android on 21/09/2017.
 */

public class PlaceResultHelper {
    public ArrayList<PlaceResultHelper.Result> results;
    public String status;

    public static class Result {
        public PlaceGeo geometry;
        public String name;
        public String place_id;
        public PlaceState opening_hours;
        public double rating;
        public String vicinity;
        public ArrayList<PlacePhoto> photos;
    }

    public static class PlaceState {
        public boolean open_now;
    }
    public static class PlaceGeo {
        public PlaceLocation location;
    }

    public static class PlaceLocation {
        public double lat;
        public double lng;
    }
    public static class PlacePhoto {
        public String photo_reference;
    }


    public PizzaPlace toModel() {
        return new PizzaPlace().setName(results.get(0).name)
                .setRatings(results.get(0).rating)
                .setOpened(results.get(0).opening_hours.open_now)
                .setLatitude(results.get(0).geometry.location.lat)
                .setLongitude(results.get(0).geometry.location.lng)
                .addImage(results.get(0).photos.get(0).photo_reference)
                .setAppstate(PizzaPlace.APPSTATE.RESULT_OK)
                .setAddress(results.get(0).vicinity);
    }

    public boolean isValid(){
        return status.equalsIgnoreCase("OK");
    }
}
