package com.oniyidewhite.android.pizzalocator.manager;

import android.util.Log;

import static com.oniyidewhite.android.pizzalocator.Constants.API.*;

import com.oniyidewhite.android.pizzalocator.Constants;
import com.oniyidewhite.android.pizzalocator.event.EventManager;
import com.oniyidewhite.android.pizzalocator.helper.PlaceDetailHelper;
import com.oniyidewhite.android.pizzalocator.helper.PlaceResultHelper;
import com.oniyidewhite.android.pizzalocator.listener.RestService;
import com.oniyidewhite.android.pizzalocator.model.PizzaPlace;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by android on 21/09/2017.
 */

public class ApiManager {
    private static final String TAG = ApiManager.class.getSimpleName();
    private static ApiManager apis;
    private RestService rest;


    private ApiManager(){
        Log.d(TAG, "init: called");
        // pass ref to to allow singleton
        apis = this;

        // declare and set up connection prefs
        // inject intercept
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .writeTimeout(120, TimeUnit.SECONDS )
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(45, TimeUnit.SECONDS)
                .build();

        // Inflate APIs using connection prefs
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        rest  = retrofit.create(RestService.class);
    }

    /**
     * Get Instance of API manger to help query Services
     * @return Ref of the API Manager
     */
    public static ApiManager getInstance() {
        return apis == null ? new ApiManager() : apis;
    }


    public void getNearestPizzaPlace(double latitude, double longitude){
        // make connection
        rest.listPizzaPlaces(
                String.format(Locale.ENGLISH,"%s,%s", latitude, longitude),
                String.valueOf(Constants.SEARCH_RADIUS),
                Constants.TYPE, Constants.KEYWORD,
                Constants.API_KEY
        ).enqueue(new Callback<PlaceResultHelper>() {
            @Override
            public void onResponse(Call<PlaceResultHelper> call, Response<PlaceResultHelper> response) {
                if(response.isSuccessful()){
                    Log.w(TAG, "isSuccessful");
                    PlaceResultHelper helper = response.body();

                    if(helper != null && helper.isValid()){
                        Log.w(TAG, "Valid ");
                        PizzaPlace place = helper.toModel();

                        // get the details (contact number and images)
                        getPizzaPlaceDetails(place, helper.results.get(0).place_id);

                    }else {
                        EventManager.getInstance().postWithSave(new PizzaPlace().setAppstate(PizzaPlace.APPSTATE.RESULT_FAILED.setMsg("Unable to read server response")), true);
                    }

                }else {
                    EventManager.getInstance().postWithSave(new PizzaPlace().setAppstate(PizzaPlace.APPSTATE.RESULT_FAILED.setMsg("An unexpected error occurred")), true);
                }
            }

            @Override
            public void onFailure(Call<PlaceResultHelper> call, Throwable t) {
                EventManager.getInstance().postWithSave(new PizzaPlace().setAppstate(PizzaPlace.APPSTATE.RESULT_FAILED.setMsg("Internet Unavailable")), true);
                t.printStackTrace();
            }
        });
    }


    private void getPizzaPlaceDetails(final PizzaPlace pizzaPlace, String placeid){
        //?placeid={place_id}&key={api_key}

        rest.getPizzaDetails(placeid, Constants.API_KEY).enqueue(new Callback<PlaceDetailHelper>() {
            @Override
            public void onResponse(Call<PlaceDetailHelper> call, Response<PlaceDetailHelper> response) {

                if(response.isSuccessful()){
                    PlaceDetailHelper helper = response.body();
                    if(helper != null && helper.isValid()){
                        Log.w(TAG, "success Details");
                        PizzaPlace place = helper.toModel(pizzaPlace);
                       // Toast.makeText(App.getInstance(),String.format(Locale.ENGLISH," %s \n %s \n rating: %s \n Images : %s", place.getName(), place.getAddress(), place.getRatings(), pizzaPlace.getImages().size()), Toast.LENGTH_LONG).show();
                        EventManager.getInstance().postWithSave(place, true);
                    }else {
                        Log.w(TAG, "failed Details");
                        EventManager.getInstance().postWithSave(new PizzaPlace().setAppstate(PizzaPlace.APPSTATE.RESULT_FAILED.setMsg("Unable to read server response")), true);
                    }


                }else {
                    EventManager.getInstance().postWithSave(new PizzaPlace().setAppstate(PizzaPlace.APPSTATE.RESULT_FAILED.setMsg("An unexpected error occurred")), true);
                }

            }

            @Override
            public void onFailure(Call<PlaceDetailHelper> call, Throwable t) {
                EventManager.getInstance().postWithSave(new PizzaPlace().setAppstate(PizzaPlace.APPSTATE.RESULT_FAILED.setMsg("Internet Unavailable")), true);
                t.printStackTrace();
            }
        });


    }

}
