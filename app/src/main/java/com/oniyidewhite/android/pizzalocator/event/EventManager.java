package com.oniyidewhite.android.pizzalocator.event;

import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.otto.ThreadEnforcer;
import com.oniyidewhite.android.pizzalocator.model.PizzaPlace;

/**
 * Created by android on 21/09/2017.
 */

public class EventManager extends Bus {

    private static EventManager eventManager;
    private PizzaPlace pizzaPlace = new PizzaPlace();


    private EventManager(ThreadEnforcer enforcer){
        super(enforcer);
        eventManager = this;
        register(this);
    }


    public static EventManager getInstance() {
        return eventManager == null ? new EventManager(ThreadEnforcer.MAIN) : eventManager;
    }

    /**
     * Init an Event to all listners
     * @param pizzaPlace
     * @param realtime
     */
    public void postWithSave(PizzaPlace pizzaPlace, boolean realtime){
        this.pizzaPlace = pizzaPlace;

        if(realtime)
            post(pizzaPlace);

    }

    @Produce
    public PizzaPlace getPizzaPlace() {
        return pizzaPlace;
    }
}
