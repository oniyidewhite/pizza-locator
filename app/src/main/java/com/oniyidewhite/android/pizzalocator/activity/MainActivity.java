package com.oniyidewhite.android.pizzalocator.activity;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.assent.AfterPermissionResult;
import com.afollestad.assent.Assent;
import com.afollestad.assent.PermissionResultSet;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.squareup.otto.Subscribe;
import com.oniyidewhite.android.pizzalocator.App;
import com.oniyidewhite.android.pizzalocator.R;
import com.oniyidewhite.android.pizzalocator.activity.base.BaseActivity;
import com.oniyidewhite.android.pizzalocator.adapter.ScreenSlidePageAdapter;
import com.oniyidewhite.android.pizzalocator.event.EventManager;
import com.oniyidewhite.android.pizzalocator.manager.ApiManager;
import com.oniyidewhite.android.pizzalocator.model.PizzaPlace;
import com.oniyidewhite.android.pizzalocator.transform.ZoomOutPageTransformer;

import java.util.List;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity  {
    // for debug
    public static String TAG = BaseActivity.class.getSimpleName();
    private static final int REQUEST_ID = 0x34;
    // if location has been granted
    private boolean usable = false;
    //first open
    private boolean firstAttempt = true;
    // we already found a coord
    // ref to gps hardware
    private GoogleApiClient mGoogleApiClient;

    // my current location
    private Location currentLocation;

    // nearest pizza place
    private PizzaPlace pizzaPlace;

    @BindString(R.string.location_msg)
    String location_message;

    @BindView(R.id.pizza_name)
    TextView placeName;

    @BindView(R.id.pizza_address)
    TextView placeAddress;

    @BindView(R.id.pizza_rating)
    TextView placeRatings;

    @BindView(R.id.slide_show_pager)
    ViewPager slideShow;

    @BindView(R.id.my_pizza)
    AppCompatImageView imageView;

    @BindView(R.id.refresh_location_grinder)
    View grinder;

    @BindView(R.id.refresh_location)
    View refresh_btn;


    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null ){
            pizzaPlace = (PizzaPlace) savedInstanceState.getSerializable("key");

            if(pizzaPlace == null)
                return;

            firstAttempt = false;
            updateUI(pizzaPlace);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(firstAttempt) {

            initProcess();

            firstAttempt = false;
        }
    }

    @AfterPermissionResult(permissions = {Assent.ACCESS_FINE_LOCATION})
    public void onPermissionResult(PermissionResultSet result) {
        Log.d(TAG, "onPermissionResult: ");
        if (result.isGranted(Assent.ACCESS_FINE_LOCATION)) {
            usable = true;
            buildGoogleApiClient();
        } else {
            usable = false;
            dialog(location_message);
        }
    }

    /**
     * Start work for GPS coords
     */
    @OnClick(R.id.refresh_location)
    public void initProcess() {
        Assent.requestPermissions(MainActivity.this, REQUEST_ID,
                Assent.ACCESS_FINE_LOCATION);
    }

    // Animate the refresh button
    private void toggleAnimation(boolean display){
        if(display) {
            grinder.setVisibility(View.VISIBLE);
            refresh_btn.setVisibility(View.GONE);
        }else {
            grinder.setVisibility(View.GONE);
            refresh_btn.setVisibility(View.VISIBLE);
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mGoogleApiClient != null && mGoogleApiClient.isConnected()){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()){
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setSmallestDisplacement(10f);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(resultCallback);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            toggleAnimation(true);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);



        }else {
            Toast.makeText(App.getInstance(),"App denied access to device location", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.w(TAG, String.format(Locale.ENGLISH, "onLocationChanged: %s", location));
        currentLocation = location;
        queryPizzas(location.getLatitude(), location.getLongitude());
    }

    private void queryPizzas(double lat, double lng){
        ApiManager.getInstance().getNearestPizzaPlace(lat, lng);
    }

    @Subscribe public void onResult(PizzaPlace pizzaPlace){
        toggleAnimation(false);

        switch (pizzaPlace.getAppstate()){

            case DO_NOT_DISTURB:
                // do nothing for now
                return;

            case RESULT_OK:
                // update the UI
                updateUI(pizzaPlace);
                break;

            case RESULT_FAILED:
                // show message
                dialog(pizzaPlace.getAppstate().getMsg());
                break;

        }

        // reset
        EventManager.getInstance().postWithSave(pizzaPlace.setAppstate(PizzaPlace.APPSTATE.DO_NOT_DISTURB), false);

    }

    private void updateUI(PizzaPlace pizzaPlace){
        this.pizzaPlace = pizzaPlace;
        placeName.setText(pizzaPlace.getName());
        placeAddress.setText(pizzaPlace.getAddress());
        placeRatings.setText(String.valueOf(pizzaPlace.getRatings()));

        // test
       // App.getInstance().getPicasso().load(String.format(Locale.ENGLISH, Constants.API.PIZZA_IMAGE_RAW, pizzaPlace.getImages().get(0), Constants.API_KEY)).fit().into(imageView);

        if (!pizzaPlace.getImages().isEmpty())
            setUpImages();

    }

    private void setUpImages(){
        slideShow.setAdapter(new ScreenSlidePageAdapter(getSupportFragmentManager(), pizzaPlace));
        slideShow.setPageTransformer(true, new ZoomOutPageTransformer());

    }

    private boolean isCallable(Intent intent) {
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    @OnClick(R.id.directions_to_pizza)
    public void goToLocation(){
        if(!usable && pizzaPlace == null)
            return;
        Uri gmmIntentUri = Uri.parse(String.format(Locale.ENGLISH, "google.navigation:q=%s,%s", pizzaPlace.getLatitude(), pizzaPlace.getLongitude()));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (isCallable(mapIntent)) {

            startActivity(mapIntent);
        }else {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                    "http://maps.google.com/maps?saddr="
                            + currentLocation.getLatitude() + ","
                            + currentLocation.getLongitude() +
                            "&daddr=" + pizzaPlace.getLatitude() + ","
                            + pizzaPlace.getLongitude()));
            startActivity(intent);
        }
    }

    @OnClick(R.id.sms_pizza)
    public void smsPizza(){
        if(!usable && pizzaPlace == null)
            return;
        Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( String.format("sms:%s", pizzaPlace.getPhone())));
        startActivity(intent);
    }

    @OnClick(R.id.call_pizza)
    public void callPizza(){
        if(!usable && pizzaPlace == null)
            return;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(Locale.ENGLISH,"tel://%s", pizzaPlace.getPhone())));
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("key", pizzaPlace);
    }
}
