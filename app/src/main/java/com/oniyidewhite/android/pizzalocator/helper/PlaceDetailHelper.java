package com.oniyidewhite.android.pizzalocator.helper;

import com.oniyidewhite.android.pizzalocator.model.PizzaPlace;

import java.util.ArrayList;

/**
 * Created by android on 21/09/2017.
 */

public class PlaceDetailHelper {
    public Results result;
    public String status;

    public static class Results {
        public String formatted_address;
        public String formatted_phone_number;
        public ArrayList<PlaceDetailsPhoto> photos;
    }

    public static class PlaceDetailsPhoto {
        public String photo_reference;
    }


    public PizzaPlace toModel(PizzaPlace pizzaPlace) {
        pizzaPlace.setAddress(result.formatted_address)
                .setPhone(result.formatted_phone_number);

        for (PlaceDetailsPhoto photo : result.photos){
            pizzaPlace.addImage(photo.photo_reference);
        }
        return pizzaPlace;
    }

    public boolean isValid(){
        return status.equalsIgnoreCase("OK");
    }
}
