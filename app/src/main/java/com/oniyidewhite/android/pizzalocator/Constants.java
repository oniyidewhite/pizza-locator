package com.oniyidewhite.android.pizzalocator;

/**
 * Created by android on 21/09/2017.
 */

public final class Constants {
    public static final String API_KEY = "AIzaSyDrKNveACGXqr_BI8HYu9kMYK9YQXq8vd4";
    public static final int SEARCH_RADIUS  = 10000;
    public static final String KEYWORD = "dominospizza";
    public static final String TYPE = "restaurant";


    public static final  class  API {
        public static final String BASE_URL = "https://maps.googleapis.com/maps/api/place/";
        public static final String LIST_PIZZAS = "nearbysearch/json"; //?location={lat},{lng}&radius={rad}&types=restaurant&key={api_key}&keyword=dominos
        public static final String PIZZA_DETAILS = "details/json"; //?placeid={place_id}&key={api_key}
        public static final String PIZZA_IMAGE = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference={photo_ref}&key={api_token}";
        public static final String PIZZA_IMAGE_RAW = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=%s&key=%s";
    }

}
