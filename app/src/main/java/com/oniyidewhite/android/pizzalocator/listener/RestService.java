package com.oniyidewhite.android.pizzalocator.listener;

import com.oniyidewhite.android.pizzalocator.helper.PlaceDetailHelper;
import com.oniyidewhite.android.pizzalocator.helper.PlaceResultHelper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.oniyidewhite.android.pizzalocator.Constants.API.*;

/**
 * Created by android on 21/09/2017.
 */

public interface RestService {

    /**
     * Get the list of Pizza places sorted by distance
     * @param radius
     * @param token
     * @return
     */
    @GET(LIST_PIZZAS)
    Call<PlaceResultHelper> listPizzaPlaces(@Query("location") String latLng,
                                            @Query("radius") String radius,
                                            @Query("types") String type,
                                            @Query("keyword") String keyword,
                                            @Query("key") String token);

    /**
     * Retrieve more info on the Pizza place
     * e.g, phone number, images
     * @param placeID
     * @param token
     * @return
     */
    @GET(PIZZA_DETAILS)
    Call<PlaceDetailHelper> getPizzaDetails(@Query("placeid") String placeID,
                                            @Query("key") String token);

    /**
     * Retrieve Pizza place image
     * e.g, phone number, images
     * this maynot be used
     * @param photoRef
     * @param token
     * @return
     */
    @GET(PIZZA_IMAGE)
    Call<ResponseBody> getPizzaImage(@Query("photo_ref") String photoRef,
                                       @Query("key") String token); //?placeid={place_id}&key={api_key}
}
