package com.oniyidewhite.android.pizzalocator.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.oniyidewhite.android.pizzalocator.App;
import com.oniyidewhite.android.pizzalocator.Constants;
import com.oniyidewhite.android.pizzalocator.R;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by android on 22/09/2017.
 */

public class ScreenSlidePageFragment extends Fragment {
    private Unbinder unbinder;
    @BindView(R.id.image_slide)
    ImageView imageView;
    private static String TAG = ScreenSlidePageFragment.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Log.w(TAG,"onCreateView");
        View view = inflater.inflate(R.layout.slide, container, false);
        unbinder = ButterKnife.bind(this, view);
        String data = getArguments().getString("key");
        if (data != null )
        App.getInstance().getPicasso().load(String.format(Locale.ENGLISH, Constants.API.PIZZA_IMAGE_RAW, data, Constants.API_KEY)).fit().into(imageView);
        else
            Log.w(TAG,"null found");
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        unbinder = null;
    }
}
