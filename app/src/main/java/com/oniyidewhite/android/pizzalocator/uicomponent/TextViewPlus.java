package com.oniyidewhite.android.pizzalocator.uicomponent;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;

import com.oniyidewhite.android.pizzalocator.R;

/**
 * Created by android on 21/09/2017.
 */

public class TextViewPlus extends android.support.v7.widget.AppCompatTextView {
    private static final String TAG = "TextView";

    public TextViewPlus(Context context) {
        super(context);
    }

    public TextViewPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public TextViewPlus(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    /**
     * Set Custom font for this view
     * @param ctx Context of the parent
     * @param attrs (not in used)
     */
    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewPlus);
        String customFont = a.getString(R.styleable.TextViewPlus_customFont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    /**
     * Set the font here
     * @param ctx of the origin
     * @param asset String of the font
     * @return boolean if was a success
     */
    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), asset);
        } catch (Exception e) {
            Log.w(TAG, e); return false;
        }
        setTypeface(tf);
        return true;
    }

}