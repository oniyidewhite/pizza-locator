package com.oniyidewhite.android.pizzalocator;

import android.app.Application;

import com.squareup.picasso.Picasso;

/**
 * Created by android on 21/09/2017.
 */

public class App extends Application {
    private static App app;
    private Picasso picasso;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        picasso = Picasso.with(app);
    }

    /**
     * Get the Application itself
     * @return Application
     */
    public static App getInstance() {
        return app;
    }

    public Picasso getPicasso() {
        return picasso;
    }
}
