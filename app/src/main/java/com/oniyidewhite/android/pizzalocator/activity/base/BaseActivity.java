package com.oniyidewhite.android.pizzalocator.activity.base;

import android.content.IntentSender;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.afollestad.assent.Assent;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.oniyidewhite.android.pizzalocator.event.EventManager;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**THis based is only used by the main activity so it saFE to use it to hold ref to unwanted implemented methods
 * Created by android on 21/09/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        com.google.android.gms.location.LocationListener,
        GoogleApiClient.OnConnectionFailedListener {


    private Unbinder unbinder;
    private MaterialDialog dialog, msgDialog;
    private static String TAG = BaseActivity.class.getSimpleName();


    /**
     * Layout Resources
     * @return the Layout to be applied
     */
    public abstract @LayoutRes int getLayoutId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        buildDialog();
        unbinder = ButterKnife.bind(this);
        Assent.setActivity(this, this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        // register event bus
        EventManager.getInstance().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Assent.setActivity(this, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing())
            Assent.setActivity(this, null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // unregister the event bus
        EventManager.getInstance().unregister(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // handles permission
        Assent.handleResult(permissions, grantResults);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialog = null;
        unbinder.unbind();
    }

    private void buildDialog(){
        dialog = new MaterialDialog.Builder(this)
                .cancelable(false)
                .theme(Theme.DARK)
                .progress(true, 0)
                .typeface(Typeface.createFromAsset(getAssets(),"Lato-Bold.ttf"),Typeface.createFromAsset(getAssets(),"Lato-Regular.ttf"))
                .content("Please wait")
                .build();
    }

    /**
     * Get progress dialog window
     * @return instance of the Dialog
     */
    public MaterialDialog getDialog() {
        return dialog;
    }


    /**
     * Display Message Dialog
     * @param msg (Message that should be displayed)
     */
    public void dialog(String msg){
        if(msgDialog == null)
            msgDialog();
        msgDialog.setContent(msg);
        msgDialog.show();
    }

    private void msgDialog(){
        msgDialog = new MaterialDialog.Builder(this)
                .cancelable(false)
                .theme(Theme.DARK)
                .negativeText("Dismiss")
                .typeface(Typeface.createFromAsset(getAssets(),"Lato-Bold.ttf"),Typeface.createFromAsset(getAssets(),"Lato-Regular.ttf"))
                .build();
    }



    @Override
    public void onConnectionSuspended(int i) {
        // not in use
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // not in use
    }


    protected ResultCallback<LocationSettingsResult> resultCallback = new ResultCallback<LocationSettingsResult>(){
        @Override
        public void onResult(@NonNull LocationSettingsResult result) {
            Log.d(TAG, "setResultCallback: Result");
            final Status status = result.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    Log.e(TAG, "LocationSettingsStatusCodes.SUCCESS");

                    // All location settings are satisfied. The client can
                    // initialize location requests here.


                    //  initiateMap();
                    //startLocationUpdates();

                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    Log.e(TAG, "LocationSettingsStatusCodes.RESOLUTION_REQUIRED");
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(
                                BaseActivity.this,
                                status.getStatusCode());
                    } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Log.e(TAG, "SETTINGS_CHANGE_UNAVAILABLE");
                    // Location settings are not satisfied. However, we have no way
                    // to fix the settings so we won't show the dialog.

                    break;
            }
        }
    };

}