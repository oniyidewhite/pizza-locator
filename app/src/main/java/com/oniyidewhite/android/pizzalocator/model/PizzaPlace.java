package com.oniyidewhite.android.pizzalocator.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A pizza place
 * Created by android on 21/09/2017.
 */

public class PizzaPlace implements Serializable{
    private String name;
    private APPSTATE appstate = APPSTATE.DO_NOT_DISTURB;
    private String address;
    private double ratings;
    private double latitude;
    private double longitude;
    private String phone;
    private boolean isOpened;
    private ArrayList<String> images = new ArrayList<>();

    public enum APPSTATE {
        RESULT_OK, RESULT_FAILED(""), DO_NOT_DISTURB;
        private String msg;
        APPSTATE(String msg){
            this.msg = msg;
        }
        APPSTATE(){
            this.msg = "undefined";
        }

        public APPSTATE setMsg(String msg) {
            this.msg = msg;
            return this;
        }

        public String getMsg() {
            return msg;
        }
    }

    public PizzaPlace setAddress(String address) {
        this.address = address;
        return this;
    }

    public PizzaPlace addImage(String image) {
        this.images.add(image);
        return this;
    }

    public PizzaPlace setOpened(boolean opened) {
        isOpened = opened;
        return this;
    }

    public PizzaPlace setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public PizzaPlace setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public PizzaPlace setName(String name) {
        this.name = name;
        return this;
    }

    public PizzaPlace setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public PizzaPlace setRatings(double ratings) {
        this.ratings = ratings;
        return this;
    }

    public PizzaPlace setAppstate(APPSTATE appstate) {
        this.appstate = appstate;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getRatings() {
        return ratings;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public APPSTATE getAppstate() {
        return appstate;
    }

    public boolean isOpened() {
        return isOpened;
    }
}
